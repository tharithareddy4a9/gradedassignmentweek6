package com.hcl.movies;

public class TestMovies {

	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub

		FactoryPatternTypeMovie movieFactory = new FactoryPatternTypeMovie();
		
	      //get an object of Circle and call its draw method.
	     IMovies comingSoonMovies = movieFactory.getMovies("MoviesComming");

	      //call draw method of Circle
	     comingSoonMovies.display();
	      
	      IMovies theatricalMovies = movieFactory.getMovies("MoviesInTheaters");
		
	      theatricalMovies.display();
	      
	    IMovies topRatedMoviesInIndia = movieFactory.getMovies("TopRatedIndiaMovies");
			
	    topRatedMoviesInIndia.display();
	      
	    IMovies topRatedMovies = movieFactory.getMovies("TopRatedMovies");
			
	    topRatedMovies.display();
	    }

}
