package com.hcl.movies.singleton;

import java.sql.Connection;
import java.sql.DriverManager;

public class SingletonPatternMovies {

	

	private static Connection conn=null;

	private SingletonPatternMovies() {
		
	}
	public static Connection getSingletonMovies() {
		try {
			if(conn==null) {
				Class.forName("com.mysql.cj.jdbc.Driver");
				conn=(Connection)DriverManager.getConnection("jdbc:mysql://localhost:3306/movies","root","Haritha@456");
				System.out.println("database connected successfully\n");
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
}
