package com.hcl.movies.singleton;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class SingletonMovie {

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
       Connection conn =SingletonPatternMovies.getSingletonMovies();
       try {
    	   String movie="select * from MoviesComing";
    	   Statement statement =conn.createStatement();
    	   ResultSet resultSet =statement.executeQuery(movie);
    	   while(resultSet.next()) {
    		   System.out.println(resultSet.getInt(1)+" "+resultSet.getString(2)+" "+resultSet.getInt(3)+" "
    				   +resultSet.getNString(4));
    	   }
    	   
    	   
           
    	   String movie1="select * from TopRatedIndia";
    	   Statement statement1 =conn.createStatement();
    	   ResultSet resultSet1 =statement1.executeQuery(movie1);
    	   while(resultSet1.next()) {
    		   System.out.println(resultSet1.getInt(1)+"  "+resultSet1.getString(2)+"  "+
    	   resultSet1.getInt(3)+"  "+resultSet1.getNString(4));
    	   }
    	   
    	  
    	   String movie2="select * from TopRatedMovies";
    	   Statement statement2 =conn.createStatement();
    	   ResultSet resultSet2 =statement2.executeQuery(movie2);
    	   while(resultSet2.next()) {
    		   System.out.println(resultSet2.getInt(1)+"  "+resultSet2.getString(2)+"  "+
    	   resultSet2.getInt(3)+"  "+resultSet2.getNString(4));
    	   }
    	   
    	  
    	   String movie3="select * from MovieInTheatres";
    	   Statement statement3 =conn.createStatement();
    	   ResultSet resultSet3 =statement3.executeQuery(movie3);
    	   while(resultSet3.next()) {
    		   System.out.println(resultSet3.getInt(1)+"  "+resultSet3.getString(2)+"  "+
    	   resultSet3.getInt(3)+"  "+resultSet3.getNString(4));
    	   }
       }catch(Exception e) {
    	   System.out.println("got some error in connection");
       }
	}

}
