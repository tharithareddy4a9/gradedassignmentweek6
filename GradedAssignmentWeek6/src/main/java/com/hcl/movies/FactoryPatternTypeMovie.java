package com.hcl.movies;

public class FactoryPatternTypeMovie {

	public IMovies getMovies(String movieType) {

		if (movieType == null) {
			return null;
		}
		if (movieType.equalsIgnoreCase("MoviesComming")) {
			return new MoviesComingSoon();
		}
		else if (movieType.equalsIgnoreCase("MoviesInTheaters")) {
			return new TheatricalRelease();
		} 
		else if (movieType.equalsIgnoreCase("TopRatedIndiaMovies")) {
			return new TopRatedMoviesInIndia();
		} 
		else if (movieType.equalsIgnoreCase("TopRatedMovies")) {
			return new TopRatedMovies();
		}
		return null;
	}

}
